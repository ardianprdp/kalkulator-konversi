package com.kalkulatorkonversi;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText inputAngka;
    private RadioButton rbMeterFeet;
    private Button btnProses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inputAngka = (EditText) findViewById(R.id.inputNilai);
        rbMeterFeet = (RadioButton) findViewById(R.id.radioToFeet);
        rbMeterFeet.setChecked(true);
        btnProses = (Button) findViewById(R.id.btnProses);

        btnProses.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                kalkulasi();
            }
        });
    }

    public void kalkulasi() {
        double meter = 3.2808399;
        double angka = 0;
        String hasil;

        try {
            angka = Double.parseDouble(inputAngka.getText().toString());
        } catch (Exception e) {
            Toast.makeText(this, "Masukkan Angka", Toast.LENGTH_LONG).show();
        }

        if(rbMeterFeet.isChecked()) {
            hasil = angka + " Meter = " + (angka * meter) + " Feet";
        } else {
            hasil = angka + " Feet = " + (angka / meter) + " Meter";
        }

        AlertDialog.Builder ab = new AlertDialog.Builder(this);
        ab.setTitle("Hasil");
        ab.setMessage(hasil);
        ab.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = ab.create();
        dialog.show();

    }
}
